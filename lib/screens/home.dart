import 'package:flutter/material.dart';

import 'ipn.dart';

class MyApp extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    TextEditingController _controller = TextEditingController();
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.indigo,
          leading: Icon(Icons.menu),
          title: Text('Inicio'),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.email),
              onPressed: null,
            ),
            IconButton(
              icon: Icon(Icons.ac_unit),
              onPressed: null,
            )
          ]),
      body: Center(
        child: Column(children: <Widget>[
          Text('¡Clicks $total!'),
          TextField(
            enabled: true,
            maxLength: 10,
            controller: _controller,
          ),
          RaisedButton(
            child: Text("Abrir imagen"),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => Ipn(cadena: _controller.text)));
            },
            color: Colors.blue,
            splashColor: Colors.yellow,
          )
        ]),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => contarClicks(),
        backgroundColor: Colors.indigo,
        child: Icon(Icons.add),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.print), title: Text('Imprimir')),
          BottomNavigationBarItem(
              icon: Icon(Icons.send), title: Text('Enviar')),
          BottomNavigationBarItem(
              icon: Icon(Icons.adjust), title: Text('Ajustar'))
        ],
        onTap: (int i) => debugPrint("Menú: $i"),
      ),
    );
  }

  int total = 0;

  contarClicks() {
    setState(() => total++);
  }
}
