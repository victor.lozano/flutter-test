import 'package:flutter/material.dart';

class Ipn extends StatelessWidget {
  Ipn({@required this.cadena}) : super();
  String cadena = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Imagen"),
        ),
        body: Center(
            child: Column(children: <Widget>[
          Text("¡Hola $cadena!"),
          RaisedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text("Regresar"),
          ),
          Image.asset("assets/images/img.png")
        ])));
  }
}
